import {DataSerializer} from "./DataSerializer.js";
import {mongoClient} from "../providers/MongoDB.js";

export class MongoSerializer extends DataSerializer {
    static async save(scope, items) {
        try {
            return await mongoClient.collection(scope).insertMany(items, {ordered: false});
        } catch (e) {
        }
    }
}
