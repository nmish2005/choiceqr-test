import {DataSerializer} from "./DataSerializer.js";
import {SPREADSHEET_ID} from "../constants.js";
import {googleSheets, auth as sheetsAuth} from '../providers/Sheets.js'
import {getMongoEstablishments} from "../middlewares/Establishment.js";
import {getMongoProducts} from "../middlewares/Product.js";

export class SheetsSerializer extends DataSerializer {
    static async getStoredDataKeys(scope) {
        switch (scope) {
            case 'establishments':
                const {data: {valueRanges: establishmentValueRanges}} = await googleSheets.spreadsheets.values.batchGet({
                        auth: sheetsAuth, //auth object
                        spreadsheetId: SPREADSHEET_ID, // spreadsheet id
                        ranges: ["Establishments!B2:B9999", "Establishments!C2:C9999"],
                    }),
                    [{values: cities}, {values: establishmentTitles}] = establishmentValueRanges;

                if (!establishmentTitles || !cities) {
                    return [];
                }

                return establishmentTitles.map((title, i) => `${cities[i][0]}->${title[0]}`);

            case 'products':
                const {data: {valueRanges: productValueRanges}} = await googleSheets.spreadsheets.values.batchGet({
                        auth: sheetsAuth, //auth object
                        spreadsheetId: SPREADSHEET_ID, // spreadsheet id
                        ranges: ["Products!E2:E9999", "Products!B2:B9999"],
                    }),
                    [{values: establishmentIds}, {values: productTitles}] = productValueRanges;

                if (!establishmentIds || !productTitles) {
                    return [];
                }

                return productTitles.map((title, i) => `${establishmentIds[i][0]}->${title[0]}`);
        }
    }

    static async save(scope, items) {
        let storedDataKeys = [];

        switch (scope) {
            case 'establishments':
                storedDataKeys = await SheetsSerializer.getStoredDataKeys(scope);

                items = items.filter(record =>
                    !storedDataKeys.includes(`${record.city}->${record.title}`));

                items = await getMongoEstablishments(items);

                await googleSheets.spreadsheets.values.append({
                    auth: sheetsAuth, //auth object
                    spreadsheetId: SPREADSHEET_ID, //spreadsheet id
                    range: "Establishments!A:F", //sheet name and range of cells
                    valueInputOption: "USER_ENTERED", // The information will be passed according to what the user passes in as date, number or text
                    resource: {
                        values: items.map(i => ([
                            i._id,
                            i.city,
                            i.title,
                            i.cuisine,
                            i.address,
                            i.phone
                        ]))
                    },
                });

                break;

            case 'products':
                storedDataKeys = await SheetsSerializer.getStoredDataKeys(scope);

                items = items.filter(record =>
                    !storedDataKeys.includes(`${record.establishmentId.toString()}->${record.title}`));

                items = await getMongoProducts(items);

                await googleSheets.spreadsheets.values.append({
                    auth: sheetsAuth, //auth object
                    spreadsheetId: SPREADSHEET_ID, //spreadsheet id
                    range: "Products!A:E", //sheet name and range of cells
                    valueInputOption: "USER_ENTERED", // The information will be passed according to what the user passes in as date, number or text
                    resource: {
                        values: items.map(i => ([
                            i.pic,
                            i.title,
                            i.description,
                            i.price,
                            i.establishmentId
                        ]))
                    },
                });
        }
    }
}
