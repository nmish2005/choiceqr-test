import {google} from "googleapis"

const auth = new google.auth.GoogleAuth({
    keyFile: "sheets.credentials.json", //the key file
    //url to spreadsheets API
    scopes: "https://www.googleapis.com/auth/spreadsheets",
});

//Auth client Object
const authClientObject = await auth.getClient();

//Google sheets instance
const googleSheets = google.sheets({version: "v4", auth: authClientObject});

export {auth, googleSheets};
