import {MongoClient} from 'mongodb'

function getMongoURL() {
    return `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DBNAME}`;
}

const client = new MongoClient(getMongoURL());

client.connect()
    .then(() => console.log('MongoDB connected'))
    .catch(e => console.log('Failed to connect MongoDB', e));

export const mongoClient = client.db(process.env.MONGO_DBNAME);
