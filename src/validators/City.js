import {LOCATIONS} from "../constants.js";

export function checkCity(req, res, next) {
    if (req.query.city && !Object.keys(LOCATIONS).includes(req.query.city)) {
        return res.status(422).json({
            type: 'Error',
            message: 'City named ' + req.query.city + ' is untracked. Please add it to constants.js -> LOCATIONS.'
        });
    }

    next();
}
