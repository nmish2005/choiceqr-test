import {ObjectId} from "mongodb";

export function isValidObjectIDInParams(key) {
    return function ObjectIDValidator(req, res, next) {
        if (!ObjectId.isValid(req.params[key])) {
            return res.status(406).json({
                type: 'Error',
                message: key + ' is not a valid ObjectID.'
            });
        }

        next();
    }
}
