export const LOCATIONS = {
    Prague: {
        lat: 50.0755381,
        lon: 14.4378005
    },
    Brno: {
        lat: 49.199385,
        lon: 16.6053
    },
    Pilsen: {
        lat: 49.7384314,
        lon: 13.3736371
    }
};

export const CUISINES = ['turkish', 'mediterranean', 'american', 'italian', 'indian', 'mexican', 'korean', 'asian', 'polish', 'thai', 'european', 'japanese', 'chinese', 'ukrainian', 'vietnamese'];

export const REQUEST_DELAY = 1000;
export const SPREADSHEET_ID = '1-Hc384AtT7Q3Rb1afn03olNctdZUhAoG23yLSIcPy9I';
