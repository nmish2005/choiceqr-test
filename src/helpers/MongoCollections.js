import {mongoClient} from "../providers/MongoDB.js";
import {MongoLogger} from "./MongoLogger.js";

export async function initCollections() {
    mongoClient.createCollection('parserQueue')
        .then(MongoLogger.onCollectionCreate('parserQueue'))
        .catch(MongoLogger.onCollectionCreateError('parserQueue'));

    mongoClient.createCollection('products')
        .then(MongoLogger.onCollectionCreate('products'))
        .catch(MongoLogger.onCollectionCreateError('products'));

    mongoClient.createCollection('establishments')
        .then(MongoLogger.onCollectionCreate('establishments'))
        .catch(MongoLogger.onCollectionCreateError('establishments'));

    mongoClient.collection('products').createIndex({title: 1, establishmentId: 1}, {unique: true})
        .then(MongoLogger.onIndexCreate('products'))
        .catch(MongoLogger.onIndexCreateError('products'));

    mongoClient.collection('establishments').createIndex({title: 1, city: 1}, {unique: true})
        .then(MongoLogger.onIndexCreate('establishments'))
        .catch(MongoLogger.onIndexCreateError('establishments'));
}
