export function capitalize(s) {
    return s.slice(0, 1).toUpperCase() + s.slice(1);
}
