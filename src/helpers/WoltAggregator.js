import {sleep} from "./Time.js"
import fetch from 'node-fetch'
import {JSDOM} from 'jsdom'
import {ResourceAggregator} from "./ResourceAggregator.js"
import {flushQueue} from "../middlewares/ExportQueue.js"
import {CUISINES, LOCATIONS, REQUEST_DELAY} from "../constants.js"
import {retrieveEstablishmentLink} from "../middlewares/Establishment.js";

export class WoltAggregator extends ResourceAggregator {
    mode = 'establishments';
    city = undefined;
    queueId = {};
    establishmentId = {};
    serializers = [];

    constructor({mode, establishmentId, city}, queueId) {
        super();

        this.city = city;
        this.mode = mode;
        this.establishmentId = establishmentId || this.establishmentId;
        this.queueId = queueId;
    }

    static get defaultHeaders() {
        return {
            "headers": {
                "accept": "application/json",
                "accept-language": "ru,en;q=0.9,uk;q=0.8,ru-UA;q=0.7,ru-RU;q=0.6,fr;q=0.5",
                "app-language": "en",
                "cache-control": "no-cache",
                "clientversionnumber": "1.7.76",
                "platform": "Web",
                "pragma": "no-cache",
                "sec-ch-ua": "\"Chromium\";v=\"104\", \" Not A;Brand\";v=\"99\", \"Google Chrome\";v=\"104\"",
                "sec-ch-ua-mobile": "?0",
                "sec-ch-ua-platform": "\"macOS\"",
                "sec-fetch-dest": "empty",
                "sec-fetch-mode": "cors",
                "sec-fetch-site": "same-site",
                "Referer": "https://wolt.com/",
                "Referrer-Policy": "strict-origin-when-cross-origin"
            }
        };
    }

    async prepareEstablishmentParser(establishmentLink) {
        await sleep(REQUEST_DELAY);

        return new JSDOM(await (await (await fetch(establishmentLink, {
            ...WoltAggregator.defaultHeaders
        })).text()));
    }

    async getPhoneNumberOfEstablishment(establishmentLink) {
        const DOM = await this.prepareEstablishmentParser(establishmentLink);

        return DOM.window.document.querySelector('[class^="AllergyInfo-module__phone"]').textContent;
    }

    async getEstablishmentsByLocation(lat, lon) {
        const rawRestaurants = (await (await fetch(`https://restaurant-api.wolt.com/v1/pages/restaurants?lat=${lat}&lon=${lon}`, {
                ...WoltAggregator.defaultHeaders
            })).json())
                .sections[1].items,
            parsedRestaurants = [];

        for (const item of rawRestaurants) {
            let phone = '';
            console.log('parsing restaurant', item.title);

            try {
                // noinspection JSDeprecatedSymbols
                // phone = (await this.getPhoneNumberOfEstablishment(item.link.target)).replaceAll(' ', '');
            } catch (e) {
            }

            parsedRestaurants.push({
                title: item.title,
                link: item.link.target,
                cuisine: item.filtering.filters[0].values.find(cat => CUISINES.includes(cat)) || 'N/I',
                address: item.venue.address,
                phone
            });
        }

        return parsedRestaurants;
    }

    async serializeParsedResults(scope, results) {
        for (const serializer of this.serializers) {
            await serializer.save(scope, results);
        }
    }

    async getProductsByEstablishment(establishmentId) {
        const establishmentLink = await retrieveEstablishmentLink(establishmentId),
            DOM = await this.prepareEstablishmentParser(establishmentLink);

        console.log('parsing products of restaurant', establishmentLink.split('/').at(-1));

        return [...DOM.window.document.querySelectorAll('[role="listitem"]')].map(el => ({
            title: el.querySelector('[data-test-id="menu-item.name"]').textContent,
            description: el.querySelector('[class^="MenuItem-module__description"]')?.textContent || '',
            price: el.querySelector('[data-test-id="menu-item-presentational.price"]')?.textContent,
            pic: el.querySelector('img')?.src,
            establishmentId
        }));
    }

    async run() {
        switch (this.mode) {
            case 'establishments':
                const establishments = [];

                for (const location of Object.entries(this.city ? {[this.city]: LOCATIONS[this.city]} : LOCATIONS)) {
                    establishments.push(...(await this.getEstablishmentsByLocation(location[1].lat, location[1].lon)).map(i => ({
                        ...i,
                        city: location[0]
                    })));

                    await sleep(REQUEST_DELAY);
                }

                await this.serializeParsedResults('establishments', establishments);
                break;

            case 'products':
                const products = [];

                products.push(...(await this.getProductsByEstablishment(this.establishmentId)));

                await this.serializeParsedResults('products', products);
        }

        await flushQueue(this.queueId, 'success');
    }
}
