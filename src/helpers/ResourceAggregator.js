export class ResourceAggregator {
    serializers = [];

    addSerializer(serializer) {
        this.serializers.push(serializer);

        return this;
    }

    async run() {
    }
}
