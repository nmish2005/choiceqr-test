export class MongoLogger {
    static onCollectionCreate(name) {
        return () => console.info('Created collection', name);
    }

    static onCollectionCreateError(name) {
        return () => console.info('Collection', name, 'already exists.');
    }

    static onIndexCreate(name) {
        return () => console.info('Created index for collection', name);
    }

    static onIndexCreateError(name) {
        return () => console.info('Index for collection', name, 'already exists.');
    }
}
