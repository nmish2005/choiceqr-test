import express from "express";
import {doesEstablishmentExist} from "../../../middlewares/Establishment.js";
import {addParserToQueue, checkExportQueue} from "../../../middlewares/ExportQueue.js";
import {MongoSerializer} from "../../../serializers/MongoSerializer.js";
import {SheetsSerializer} from "../../../serializers/SheetsSerializer.js";
import {WoltAggregator} from "../../../helpers/WoltAggregator.js";
import {isValidObjectIDInParams} from "../../../validators/ObjectID.js";

const router = express.Router();

router.post('/:establishmentId', isValidObjectIDInParams('establishmentId'), checkExportQueue, doesEstablishmentExist, addParserToQueue, (req, res) => {
    (new WoltAggregator({
        mode: 'products',
        establishmentId: req.params.establishmentId
    }, req.queueId)).addSerializer(MongoSerializer).addSerializer(SheetsSerializer).run();

    res.status(202).json({statusMsg: 'Export task was added to queue.', queueID: req.queueId});
});

export const productRouter = router;
