import express from "express";
import {establishmentRouter} from "./establishments.js";
import {productRouter} from "./products.js";

const router = express.Router();

router.use('/products', productRouter);
router.use('/establishments', establishmentRouter);

export const exportRouter = router;
