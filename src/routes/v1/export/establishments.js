import express from "express";
import {checkCity} from "../../../validators/City.js";
import {addParserToQueue, checkExportQueue} from "../../../middlewares/ExportQueue.js";
import {MongoSerializer} from "../../../serializers/MongoSerializer.js";
import {SheetsSerializer} from "../../../serializers/SheetsSerializer.js";
import {WoltAggregator} from "../../../helpers/WoltAggregator.js";

const router = express.Router();

router.post('/', checkCity, checkExportQueue, addParserToQueue, (req, res) => {
    (new WoltAggregator({
        mode: 'establishments',
        city: req.query.city
    }, req.queueId)).addSerializer(MongoSerializer).addSerializer(SheetsSerializer).run();

    res.status(202).json({type: 'Success', message: 'Export task was added to queue.', queueID: req.queueId});
});

export const establishmentRouter = router;
