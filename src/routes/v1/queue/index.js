import express from "express";
import {isValidObjectIDInParams} from "../../../validators/ObjectID.js";
import {getQueueByID} from "../../../middlewares/ExportQueue.js";

const router = express.Router()

router.get('/:exportQueueId', isValidObjectIDInParams('exportQueueId'), async (req, res) => {
    res.json(await getQueueByID(req.params.exportQueueId));
})

export const queueRouter = router;
