import express from "express";
import {exportRouter} from "./export/index.js";
import {queueRouter} from "./queue/index.js";

const router = express.Router();

router.use('/export', exportRouter);
router.use('/queue', queueRouter);

export const V1Router = router;
