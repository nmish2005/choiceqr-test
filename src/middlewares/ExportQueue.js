import {mongoClient} from "../providers/MongoDB.js";
import {ObjectId} from "mongodb";

export async function checkExportQueue(req, res, next) {
    if (await mongoClient.collection('parserQueue').countDocuments({status: 'running'}) > 0) {
        return res.status(425).json({
            type: 'Error',
            message: 'Parser is already running. Please wait it to finish and then trigger again'
        });
    }

    next();
}

export async function addParserToQueue(req, res, next) {
    req.queueId = (await mongoClient.collection('parserQueue').insertOne({
        status: 'running',
        scope: req.params.establishmentId ? 'products' : 'establishments'
    })).insertedId;

    console.log('Started queue #' + req.queueId.toString());

    next();
}

export async function flushQueue(_id, status) {
    console.log('Finished queue #' + _id.toString());
    return mongoClient.collection('parserQueue').updateOne({_id}, {$set: {status}});
}

export async function getQueueByID(_id) {
    return mongoClient.collection('parserQueue').findOne({_id: new ObjectId(_id)});
}
