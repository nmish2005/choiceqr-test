import {mongoClient} from "../providers/MongoDB.js";
import {ObjectId} from "mongodb";

export async function getMongoProducts(items) {
    return (await mongoClient.collection('products').find({
        title: {$in: items.map(i => i.title)},
        establishmentId: {$in: items.map(i => new ObjectId(i.establishmentId))}
    })).toArray();
}
