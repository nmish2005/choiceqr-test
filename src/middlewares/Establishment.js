import {mongoClient} from "../providers/MongoDB.js";
import {ObjectId} from "mongodb";

export async function doesEstablishmentExist(req, res, next) {
    if (await mongoClient.collection('establishments').countDocuments({_id: new ObjectId(req.params.establishmentId)}) === 0) {
        return res.status(422).json({
            type: 'Error',
            message: 'Establishment #' + req.params.establishmentId + ' does not exist. You may have to run the establishment parser first.'
        });
    }

    req.params.establishmentId = new ObjectId(req.params.establishmentId);

    next();
}

export async function retrieveEstablishmentLink(_id) {
    return (await mongoClient.collection('establishments').findOne({_id})).link;
}

export async function getMongoEstablishments(items) {
    return (await mongoClient.collection('establishments').find({
        title: {$in: items.map(i => i.title)},
        city: {$in: items.map(i => i.city)}
    })).toArray();
}
