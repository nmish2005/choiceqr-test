import express from 'express';

import './helpers/Config.js';
import {router} from './routes/index.js';
import {initCollections} from "./helpers/MongoCollections.js";

const app = express();

app.use(router);

await initCollections();

app.listen(process.env.PORT, () => console.log('The app is running on http://localhost:' + process.env.PORT));
