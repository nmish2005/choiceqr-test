# ChoiceQR establishment aggregator
## Setup:
1. Fill src/.env
2. Fill src/sheets.credentials.json
3. Import Postman collection (optional)
4. `docker compose up -d`
